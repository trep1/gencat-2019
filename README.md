# Projecte Gencat

Repositori pel projecte d'interfícies.

# Link 

https://bitbucket.org/trep1/gencat-2019/src/master/

## Autors: 

* Alex Estrada
* Adri Postigo
* Deniz Agisuna

## Objectiu:

Amb el document html de presentació de l'institut (https://www.institutausiasmarch.cat/pam/presentacio-2/ ) , fer el CSS per que tingui l'aparença d'una web de la Generalitat (basant-vos en la informació de la guia d'estil de la Generalitat). Afegir una noticia com a destacada queporti a una llista de professors i cada professor un enllaç a la pàgina del professor.

## Estat:

Començant a estructurar l'aplicació. Estructura de components individuals.


